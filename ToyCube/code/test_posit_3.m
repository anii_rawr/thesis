addpath(genpath('../flann-1.8.4-src'));
close all;
clear all;

%=================
imagenum = 1;
%=================

[F,V,N] = stlread('../ToyCube.stl');
Ms = V([1,2,3,6,11,15,21,24], :);

im(:, :, :, 1) = imread(strcat('../images/', 'CU0_L-1()_Lo1_Po1_PrY_OcN_BlN_BaN_LiR_RX_SP0_EPX_PPX_EX_AX_SR0.00.jpg'));
im(:, :, :, 2) = imread(strcat('../images/', 'CU0_L-1()_Lo1_Po1_PrY_OcN_BlN_BaN_LiR_RX_SP1_EPX_PPX_EX_AX_SR-0.17.jpg'));

% K = importdata('../camera_intrinsics.txt', ' ', 0);
K = [   1.6766939e+03   0.0000000e+00   6.3316483e+02
   0.0000000e+00   1.6767818e+03   4.9081245e+02
   0.0000000e+00   0.0000000e+00   1.0000000e+00];

% center = [size(im, 2)/2, size(im, 1)/2];
center = [K(1, 3), K(2, 3)];

focalLength = K(1, 1);

ps(:, :, 1) = [959.9133 277.6225;
       695.1068 221.0833;
       988.7587 544.41;
       809.9602 404.7603;
       706.0370 503.1576;
       574.6265 363.7979;
       576.1644 611.6905;
       825.5950 640.6350];
   
ps(:, :, 2) = [  977.7156  310.3124
  716.3444  209.2093
  962.4059  569.6074
  823.4010  432.9075
  677.3388  475.2420
  592.0815  352.0469
  549.8930  587.7998
  799.4921  662.9482];
        
    
C(:, :, 1) = [   9.2059755e-01  -1.9037241e-02   3.9004838e-01   3.5566417e+01
   1.3469319e-01  -9.2203917e-01  -3.6290703e-01  -1.1790818e+01
   3.6654863e-01   3.8662819e-01  -8.4626281e-01   4.5108585e+02
   0.0000000e+00   0.0000000e+00   0.0000000e+00   1.0000000e+00];

C(:, :, 2) = [   9.0741454e-01   1.4080346e-01   3.9594600e-01   3.4223667e+01
   2.9497474e-01  -8.8449144e-01  -3.6147586e-01  -1.0661509e+01
   2.9931380e-01   4.4480252e-01  -8.4413386e-01   4.5004503e+02
   0.0000000e+00   0.0000000e+00   0.0000000e+00   1.0000000e+00];

model_keypts = [16.75 -19.75 41.50;
                -32 32, 41.50;
                -38.7237 0, -15;
                5.5 -37.8698, -15];

keypts = [906 465;
        720.4 246.6;
        611 447.7;
        765.1 599.8];

P(:, :, 1) = K*C(1:3,1:4, 1);
P(:, :, 2) = K*C(1:3,1:4, 2);

Point_image = P(:, :, imagenum)*[Ms, ones(size(Ms,1),1)]';
Point_image = Point_image(1:2,:) ./ repmat(Point_image(3,:),[2,1]);
x = Point_image(1, :)';
y = Point_image(2, :)';

Point_image = P(:, :, imagenum)*[model_keypts, ones(size(model_keypts,1),1)]';
Point_image = Point_image(1:2,:) ./ repmat(Point_image(3,:),[2,1]);
x_keypt = Point_image(1, :)';
y_keypt = Point_image(2, :)';

imshow(im(:, :, :, imagenum));
axis image;
hold on;
scatter(x, y, 'ro', 'LineWidth', 5);
scatter(keypts(:, 1), keypts(:, 2), 'go', 'LineWidth', 5);
scatter(x_keypt, y_keypt, 'r.', 'LineWidth', 5);

%======================

% [R, T] = modernPosit(ps(3:8, :, imagenum), Ms(3:8, :), focalLength, center);
[R, T] = modernPosit(keypts, model_keypts, focalLength, center);

%=======================

P = K * [R T];
q = P * [Ms'; ones(1, size(Ms, 1))];
q = (q(1:2, :) ./ repmat(q(3, :), 2, 1))';

plot(q(:, 1), q(:, 2), 'b.', 'LineWidth', 5);