addpath(genpath('../flann-1.8.4-src'));
close all;
clear all;
[F,V,N] = stlread('../ToyCube.stl');

Ms = V([1,2,11,3,6,15,21,24], :)';

K = importdata('../camera_intrinsics.txt', ' ', 0);
Point_obj = [Ms; ones(1, size(Ms, 2))];

ps = [959.9133 277.6225;
       695.1068 221.0833;
       706.0370 503.1576;
       988.7587 544.41;
       809.9602 404.7603;
       574.6265 363.7979;
       576.1644 611.6905
       825.5950 640.6350]';

model_keypts = [16.75 -19.75 41.50;
                16.25 -14.5, 41.50;
                10.5 -3.25, 41.50];

keypts = [906.4289 464.9809;
        902.9605 445.3275;
        879.4526 400.9136];
    
keypts_2 = [630.7212  323.8119;
              872.6370  592.6222;
              635.0911  532.8196
            ];
    
model_keypts_2 = [-38.7237 30 0;
                  30 -37.8698 0;
                  -38.7237 -30 0];
              
keypts_3 = [ 703.3118  432.8267;
              778.1222  513.6754;
              706.0371  503.1574
            ];              
              
model_keypts_3 = [-38.7237 -20 41.50;
                  -20 -37.8698 41.50;
                  -38.7237 -37.8698 41.50];
        
image_name = 'CU0_L-1()_Lo1_Po1_PrY_OcN_BlN_BaN_LiR_RX_SP0_EPX_PPX_EX_AX_SR0.00.jpg';
im = imread(strcat('../images/', image_name));
extrinsic_name = strcat('../extrinsics/', 'CU0_L-1()_Lo1_Po1_PrY_OcN_BlN_BaN_LiR_RX_SP0_EPX_PPX_EX_AX_SR0.00_extrinsics.txt');
C = importdata(extrinsic_name,' ',0);

P = K*C(1:3,1:4);
Point_image = P*[Ms', ones(size(Ms',1),1)]';
Point_image = Point_image(1:2,:) ./ repmat(Point_image(3,:),[2,1]);
x = Point_image(1, :)';
y = Point_image(2, :)';

P = K*C(1:3,1:4);
keypt_image = P*[model_keypts_3, ones(size(model_keypts_3,1),1)]';
keypt_image = keypt_image(1:2,:) ./ repmat(keypt_image(3,:),[2,1]);
x_keypt = keypt_image(1, :)';
y_keypt = keypt_image(2, :)';

imshow(im);
axis image;
hold on;
scatter(x, y, 'ro');
plot(keypts_3(:, 1), keypts_3(:, 2), 'co', 'LineWidth', 1);
plot(x_keypt, y_keypt, 'b.', 'LineWidth', 1);
hold off;

%======================
%     %Letters P A R (works)
%     M0 = model_keypts(1,1:3)'; M1 = model_keypts(2,1:3)'; M2 =  model_keypts(3,1:3)';
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, keypts(1,:)', keypts(2,:)', keypts(3,:)');
    
    %     %Letters R A P 
%     M0 = model_keypts(3,1:3)'; M1 = model_keypts(2,1:3)'; M2 =  model_keypts(1,1:3)';
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, keypts(3,:)', keypts(2,:)', keypts(1,:)');
    
    %     %Letters P R A (works)
%     M0 = model_keypts(1,1:3)'; M1 = model_keypts(3,1:3)'; M2 =  model_keypts(2,1:3)';
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, keypts(1,:)', keypts(3,:)', keypts(2,:)');

%     %Letters A R P (works)
%     M0 = model_keypts(2,1:3)'; M1 = model_keypts(3,1:3)'; M2 =  model_keypts(1,1:3)';
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, keypts(2,:)', keypts(3,:)', keypts(1,:)');

%     %Corners 1 2 3
%     M0 = Ms(:,1); M1 = Ms(:,2); M2 = Ms(:,3);
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,1), ps(:,2), ps(:,3));

%     %Corners 3 2 1 (works)
%     M0 = Ms(:,3); M1 = Ms(:,2); M2 = Ms(:,1);
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,3), ps(:,2), ps(:,1));

%     %Corners 3 1 2 (works)
%     M0 = Ms(:,3); M1 = Ms(:,1); M2 = Ms(:,2);
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,3), ps(:,1), ps(:,2));

%     %Corners 2 3 1
%     M0 = Ms(:,2); M1 = Ms(:,3); M2 = Ms(:,1);
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,2), ps(:,3), ps(:,1));

%     %Corners 2 1 3 (works)
%     M0 = Ms(:,2); M1 = Ms(:,1); M2 = Ms(:,3);
%     [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,2), ps(:,1), ps(:,3));

%Farthest 3 corners - 7 2 4
% M0 = Ms(:,7); M1 = Ms(:,2); M2 = Ms(:,4);
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,7), ps(:,2), ps(:,4));

%Farthest 3 corners reversed - 4 2 7
% M0 = Ms(:,4); M1 = Ms(:,2); M2 = Ms(:,7);
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,4), ps(:,2), ps(:,7));

%3 corners - 2 4 7 (works)
% M0 = Ms(:,2); M1 = Ms(:,4); M2 = Ms(:,7);
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,2), ps(:,4), ps(:,7));

%3 corners - 2 7 4 (works)
% M0 = Ms(:,2); M1 = Ms(:,7); M2 = Ms(:,4);
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,2), ps(:,7), ps(:,4));

%3 corners - 4 7 2 (works)
% M0 = Ms(:,4); M1 = Ms(:,7); M2 = Ms(:,2);
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,4), ps(:,7), ps(:,2));

%3 corners - 7 4 2
% M0 = Ms(:,7); M1 = Ms(:,4); M2 = Ms(:,2);
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, ps(:,7), ps(:,4), ps(:,2));

%3 mid points
% M0 = model_keypts_2(1,1:3)'; M1 = model_keypts_2(2,1:3)'; M2 =  model_keypts_2(3,1:3)';
% [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, keypts_2(1,:)', keypts_2(2,:)', keypts_2(3,:)');

%3 closest corner points
M0 = model_keypts_3(1,1:3)'; M1 = model_keypts_3(2,1:3)'; M2 =  model_keypts_3(3,1:3)';
[P0, P1, P2, s] = weakPerspInv(M0, M1, M2, keypts_3(1,:)', keypts_3(2,:)', keypts_3(3,:)');

%=======================

[R, T] = compute3DAligningTransform([M0, M1, M2], [P0, P1, P2], [0;0;0]);

Qs = s*(R*Ms + repmat(T,1,size(Ms,2)));

% Display the backprojected model:
figure; h = image(im); hold on;
axis image;

scatter(x, y, 'bo');
% plot(keypts(:, 1), keypts(:, 2), 'r.', 'LineWidth', 1);
% plot(x_keypt, y_keypt, 'g.', 'LineWidth', 1);

x_line = [x(1), x(2), x(3), x(4), x(1), x(5), x(6), x(7), x(8), x(5), x(8), x(4), x(3), x(7), x(6), x(2)];
y_line = [y(1), y(2), y(3), y(4), y(1), y(5), y(6), y(7), y(8), y(5), y(8), y(4), y(3), y(7), y(6), y(2)];

line(x_line, y_line);

plot3(Qs(1,:), Qs(2,:), Qs(3,:), 'or'); text(Qs(1,:), Qs(2,:), Qs(3,:), num2str((1:size(Qs,2))'), 'Color', 'w', 'LineWidth', 2);
X_line = [Qs(1, 1), Qs(1, 2), Qs(1, 3), Qs(1, 4), Qs(1, 1), Qs(1, 5), Qs(1, 6), Qs(1, 7), Qs(1, 8), Qs(1, 5), Qs(1, 8), Qs(1, 4), Qs(1, 3), Qs(1, 7), Qs(1, 6), Qs(1, 2)];
Y_line = [Qs(2, 1), Qs(2, 2), Qs(2, 3), Qs(2, 4), Qs(2, 1), Qs(2, 5), Qs(2, 6), Qs(2, 7), Qs(2, 8), Qs(2, 5), Qs(2, 8), Qs(2, 4), Qs(2, 3), Qs(2, 7), Qs(2, 6), Qs(2, 2)];
Z_line = [Qs(3, 1), Qs(3, 2), Qs(3, 3), Qs(3, 4), Qs(3, 1), Qs(3, 5), Qs(3, 6), Qs(3, 7), Qs(3, 8), Qs(3, 5), Qs(3, 8), Qs(3, 4), Qs(3, 3), Qs(3, 7), Qs(3, 6), Qs(3, 2)];

line(X_line, Y_line, Z_line, 'Color', 'r');

set(h,'AlphaData',ones(size(im,1),size(im,2)) * 0.90);

error = 0;
for i = 1:8
%     error = error + (
end