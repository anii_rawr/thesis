addpath(genpath('../flann-1.8.4-src'));
close all;
% clear all;

[F,V,N] = stlread('../ToyCube.stl');
Ms = V([1,2,3,6,11,15,21,24], :);

im = imread(strcat('../images/', 'CU0_L-1()_Lo1_Po1_PrY_OcN_BlN_BaN_LiR_RX_SP0_EPX_PPX_EX_AX_SR0.00.jpg'));
center = [size(im, 2)/2, size(im, 1)/2];

K = importdata('../camera_intrinsics.txt', ' ', 0);

focalLength = K(1, 1);

ps = [959.9133 277.6225;
       695.1068 221.0833;
       988.7587 544.41;
       809.9602 404.7603;
       706.0370 503.1576;
       574.6265 363.7979;
       576.1644 611.6905;
       825.5950 640.6350];
        
    
C = [   9.2059755e-01  -1.9037241e-02   3.9004838e-01   3.5566417e+01
   1.3469319e-01  -9.2203917e-01  -3.6290703e-01  -1.1790818e+01
   3.6654863e-01   3.8662819e-01  -8.4626281e-01   4.5108585e+02
   0.0000000e+00   0.0000000e+00   0.0000000e+00   1.0000000e+00];

P = K*C(1:3,1:4);
Point_image = P*[Ms, ones(size(Ms,1),1)]';
Point_image = Point_image(1:2,:) ./ repmat(Point_image(3,:),[2,1]);
x = Point_image(1, :)';
y = Point_image(2, :)';

imshow(im);
axis image;
hold on;
scatter(x, y, 'ro');

%======================

[R, T] = modernPosit(ps(3:8, :), Ms(3:8, :), focalLength, center);

%=======================

P = K * [R T];
q = P * [Ms'; ones(1, size(Ms, 1))];
q = (q(1:2, :) ./ repmat(q(3, :), 2, 1))';

plot(q(:, 1), q(:, 2), 'bo');