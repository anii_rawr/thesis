im = imread(strcat('../images/', 'CU0_L-1()_Lo1_Po1_PrY_OcN_BlN_BaN_LiR_RX_SP0_EPX_PPX_EX_AX_SR0.00.jpg'));
image(im); hold on;

K = [1.6766939e+03   0.0000000e+00   6.3316483e+02
   0.0000000e+00   1.6767818e+03   4.9081245e+02
   0.0000000e+00   0.0000000e+00   1.0000000e+00];

ps = [959.9133 277.6225;
   695.1068 221.0833;
   988.7587 544.41];

ms = [-10 10  10;
      10  10 10;
      10  -10  10];

plot(ps(:, 1), ps(:, 2), 'r.');

[R, T] = modernPosit(ps, ms, K(1, 1), [640 512]);
P = K * [R T];
q = P * [ms'; ones(1, size(ms, 1))];
q = (q(1:2, :) ./ repmat(q(3, :), 2, 1))';

plot(q(:, 1), q(:, 2), 'bo');