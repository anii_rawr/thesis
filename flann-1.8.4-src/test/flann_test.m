% create random dataset and test set
dataset = single(rand(128,10000));
testset = single(rand(128,1000));
% define index and search parameters

params.algorithm = 'kdtree';
params.trees = 8;
params.checks = 64;
% perform the nearest-neighbor search
[result, dists] = flann_search(dataset,testset,5,params);

[dataset_loc, dataset] = get_keypts(1, 0, 0);
[testset_loc, testset] = get_keypts(1, 10, 0);

params.algorithm = 'kdtree';
params.trees = 8;
params.checks = 64;
[index, parameters, speedup] = flann_build_index(dataset, params);
flann_save_index(index, 'index');
flann_free_index(index);
clear index;
index = flann_load_index('index', dataset);

[result, dists] = flann_search(index, testset, 3, params);
