function [keypt_loc, keypts] = get_keypts(obj_num, az, el)
    keypoints_dir = 'keypoints';

    %Parse keypoints file
    keypoint_file = strcat(keypoints_dir, '\car_', int2str(obj_num), '_az_', int2str(az), '_el_', int2str(el), '.key');
    f = fopen(keypoint_file);

    line = str2double(strread(fgets(f),'%s','delimiter',' ')); %First line that says the num of keypoints and their size
    keypt_loc = zeros(line(1), 2); %num_keypts x 2 (x, y coords)
    keypts = zeros(line'); %num_keypts x 128

    keypt_i = 0;
    temp_keypt = [];
    while ~feof(f)
        line = str2double(strread(fgets(f),'%s','delimiter',' '));
        if(size(line, 1) == 4)
            keypt_i = keypt_i + 1;
            row_2d = round(line(1));
            col_2d = round(line(2));
            keypt_loc(keypt_i, :) = [row_2d, col_2d];
        else
            temp_keypt = [temp_keypt; line];
            if(length(temp_keypt) == 128)
                keypts(keypt_i, :) = temp_keypt;
                temp_keypt = [];
            end
        end
    end
    fclose(f);

end