function visualize_landmark(points,visibility,landmark,color)

temp = zeros(length(points),1);
temp(landmark) = 1;


if isempty(visibility)
    points_tmp = points(temp==1,:);
    plot(points_tmp(:,1),points_tmp(:,2),'o','MarkerFaceColor', ...
    color,'Linewidth',1,'MarkerSize',12,'MarkerEdgeColor','k');
else
    points_tmp = points(temp==1 & visibility==1,:);
    plot(points_tmp(:,1),points_tmp(:,2),'o','MarkerFaceColor', ...
    color,'Linewidth',1,'MarkerSize',12,'MarkerEdgeColor','k');

    points_tmp = points(temp==1 & visibility==0,:);
    plot(points_tmp(:,1),points_tmp(:,2),'o','Linewidth',1,'MarkerSize',12,'MarkerEdgeColor','w');
end

%text(points(landmark,1),points(landmark,2), ... 
%num2str(landmark),'BackgroundColor',color,'FontSize',12);