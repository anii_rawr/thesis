function model_coord = convert_2d_to_3d_transform_bbox(image_coord, mesh, P, K, R, T, sc)

    model_coord = zeros(size(image_coord, 1), 3);
    % project in image
      pts_3D = P * [mesh.vertices'; ones(1,size(mesh.vertices',2))];
      % scale projected points
      pts_3D(1,:) = pts_3D(1,:)./pts_3D(3,:);
      pts_3D(2,:) = pts_3D(2,:)./pts_3D(3,:);
      pts_3D(3,:) = pts_3D(2,:)./pts_3D(3,:);
      pts_3D = pts_3D';
    
%      mesh.vertices = mesh.vertices*[cosd(az),sind(az),0;-sind(az),cosd(az),0;0,0,1]; %rotate the car matrix
    
    for i = 1:size(image_coord, 1)
        Z = depthim(image_coord(i, 1), image_coord(i, 2));
        X = ((image_coord(i, 2) - K(1, 3))*Z)/K(1, 1);
        Y = ((image_coord(i, 1) - K(2, 3))*Z)/K(2, 2);
        model_coord(i, :) = [X Z Y];
    end
    
end

function [mesh, ind] = findtransform(caddata, vertices)

    vertices_cad = caddata.mesh.bbox.vertices;
    [pointsCAD, ind] = getboxpoints(vertices_cad, caddata.mesh.bbox.faces);
    
    pointsdata = vertices;
    [d, p, transf] = procrustes(pointsdata,pointsCAD);
    transform = struct('t', transf.c(1, :), 'R', transf.T, 'sc', transf.b);
    mesh = caddata.mesh;
    mesh.transform{1} = transform;
    mesh = transform_mesh2(mesh);
    
end

function [vertices, vert_ind] = getboxpoints(vertices, faces)

faces_annot = faces_for_box();
vert_ind = zeros(size(vertices, 1), 1);

for i = 1 : size(vertices, 1)
    [y,x] = find(faces_annot == i);
    y = unique(y);
    
    vert = [1:size(vertices, 1)]';
    for j = 1 : length(y)
        vert = intersect(vert, faces(y(j), :));
    end;
    vert_ind(i) = vert;
end;

vertices = vertices(vert_ind, :);
end

