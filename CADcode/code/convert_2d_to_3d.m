function model_coord = convert_2d_to_3d(image_coord, mesh, P, K, R, T, sc, el, az)

    model_coord = zeros(size(image_coord, 1), 3);
    
%     vertices = (rotz(360-az) * mesh.vertices')';
%     mesh.vertices = vertices;
    [segimout, depthim, points] = getMaskFromCAD(P, mesh, [375, 1242], el, az);
   
    x_a = image_coord(:, 2);
    y_a = image_coord(:, 1);
    
    for i = 1:size(x_a, 1)
        Y(i) = depthim(round(y_a(i)), round(x_a(i)));
    end 
    Y_a = Y';
    
    w_a = sym('w', [size(x_a, 1), 1]);
    X_a = sym('X', [size(x_a, 1), 1]);
    Z_a = sym('Y', [size(x_a, 1), 1]);
    
    for i = 1:size(x_a, 1)
        w = w_a(i);
        x = x_a(i);
        y = y_a(i);
        Z = Z_a(i);
        X = X_a(i);
        Y = Y_a(i);
        q = [w'.*x'; w'.*y'; w'];
        Q = [X'; Y'; Z'; 1];
        a = q(1) == (P(1, :) * Q);
        b = q(2) == (P(2, :) * Q);
        c = q(3) == (P(3, :) * Q);
        [w_a(i), X_a(i), Z_a(i)] = solve(a, b, c, w, X, Z);
    end
    
    w = double(w_a);
    X = double(X_a);
    Y = Y_a;
    Z = double(Z_a);
    X = changem(X, [NaN], [Inf]);
    Y = changem(Y, [NaN], [Inf]);
    Z = changem(Z, [NaN], [Inf]);
    
    model_coord = [X Y Z];
end