function [segimout, depthim, points] = getMaskFromCAD(K, mesh, imsize, el, az)

%[x, y] = project_points(K, mesh.vertices);
pp = projectToImage(mesh.vertices',K);
pp = pp';
x = pp(:, 1); y = pp(:, 2);
faces = double(mesh.faces);

segimout = zeros(imsize(1:2));
depthim = inf * ones(imsize(1:2));
[X,Y] = meshgrid(1:imsize(2), 1:imsize(1));

vertices = (rotz(az) * mesh.vertices')';
mesh.vertices = vertices;

Z = mxgridtrimesh(faces,[x,y,mesh.vertices(:, 2)],X,Y);
ind = find(~isnan(Z));
segimout(ind) = 1;
depthim(ind) = Z(ind);
points = [x, y];