% written by sanja fidler
cd 'C:\Users\Annie\Dropbox\computer vision\CADcode';
annotation = 1;
CLASS_NAME = 'car';
dataset_globals;
model_dir = CAD_MODELS_PATH;

files = dir(fullfile(model_dir, '*_mesh.mat'));
n_objects = length(files);
draw_points = 0;
show_texture = 1;
show_faces = 1;
show_axes = 1;

width = 1100;
height = 900;
h_f = figure('Position',[150 80 width height]);
set(h_f, 'Color', [0, 0, 0])
h_a = axes('Position',[0.01 0.04 0.99 1]); axis off;

obj_num = 1;

data = load(fullfile(model_dir, files(obj_num).name));
mesh = data.mesh; if isfield(data, 'mesh_viz'), mesh_viz = data.mesh_viz; else mesh_viz = []; end;

if isfield(data, 'model_file'), model_file = data.model_file; else model_file = fullfile(model_dir, files(obj_num).name); end;

% hack to get the name out
[path,name,ext] = fileparts(model_file);
orfiles = dir(strrep(path, '3ds', 'skp'));
instance_label = '';
if isfield(mesh, 'instance_name'), instance_label = mesh.instance_name; end;

for obj = (0:0),
%for obj = (0:n_objects),
    %for el = (60:10:60),
    for el = (0:10:360),
    %for el = (0:0),
       %for az = (140:10:140),
       for az = (0:10:360),
        camera.viewpoint = [az, el]
        h = viz_3D_image(mesh, camera, draw_points, show_faces, show_texture, show_axes);
        %pause;
        pause(1);
        img = getframe(gcf);
        %imwrite(img.cdata, [strcat('images/car_', int2str(obj_num), '_az_', int2str(az), '_el_', int2str(el)), '.jpg']);
        %saveas(gcf, strcat('images/car_', int2str(obj_num), '_az_',
        %int2str(az), '_el_', int2str(el)), 'jpg')
        test = gcf
        gcf
       end
    end
    
    obj_num = obj_num+1
    data = load(fullfile(model_dir, files(obj_num).name));
    mesh = data.mesh;
    if isfield(data, 'mesh_viz'),
        mesh_viz = data.mesh_viz; 
    else mesh_viz = [];
    end
    if isfield(mesh, 'instance_name'),
        instance_label = mesh.instance_name;
    else
        instance_label ='''';
    end

end
