%Demo for mapping the values from the keypoint txt files back onto the 2d
%image

addpath(genpath('../code'));

%test
az = 80;
el = 0; 
obj_num = 1;

[keypoint_loc, keypoints] = get_keypts(obj_num, az, el);

I = imread('images\jpg\car_1_az_80_el_0.jpg');
imshow(I);
axis on;
hold on;
scatter(keypoint_loc(:, 2), keypoint_loc(:, 1), 3, [1, 0, 0], 'go', 'LineWidth', 5)