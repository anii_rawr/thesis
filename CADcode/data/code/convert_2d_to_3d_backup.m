function model_coord = convert_2d_to_3d_backup(image_coord, mesh, P, K, R, T, sc)

    model_coord = zeros(size(image_coord, 1), 3);
%     [segimout, depthim, points] = getMaskFromCAD(P, mesh, [375, 1242]);
    [segimout, depthim, points] = getMaskFromCAD(P, mesh, [375, 1242]);
    % x = f_x*X/Z + p_x
    % y = f_y*Y/Z + p_y

    f_x = K(1, 1);
    f_y = K(2, 2);
    p_x = K(1, 3);
    p_y = K(2, 3);
    vertices = mesh.vertices;
%     vertices = [sc*(R') T] * [vertices';ones(1, size(vertices, 1))];
    vertices = (sc*(R')) * vertices';
    vertices = vertices + repmat(T, 1, size(vertices, 2));
    vertices = vertices';
    X = vertices(:, 1);
    Y = vertices(:, 2);
    Z = vertices(:, 3);
    x = (f_x*X)./Z + p_x;
    y = (f_y*Y)./Z + p_y;
    plot(x, y, 'ro');
    
    x_1 = x;
    y_1 = y;
    z_1 = Z;
    X_1 = X;
    Y_1 = Y;
    
    for i = 1:size(x_1, 1)
%         x = (x_1(i));
%         y = (y_1(i));
%         Z = (z_1(i));
        x = round(x_1(i));
        y = round(y_1(i));
        Z = depthim(y, x);
%         X = (((x - p_x)*Z)/f_x);
%         Y = (((y - p_y)*Z)/f_y);
        h = inv(K) * [x*Z, y*Z, Z]';
        model_coord(i, :) = h';
    end
    
    model_coord = model_coord' - repmat(T, 1, size(model_coord, 1));
    model_coord = inv(sc*(R')) * model_coord;
    model_coord = model_coord';
    
%     pts_3D = [mesh.vertices ones(size(mesh.vertices, 1), 1)];
    vertices = (P * [mesh.vertices' ; ones(size(mesh.vertices, 1), 1)'])';
    vertices_flat = vertices ./ repmat(vertices(:, 3), 1, 3);
%     [segimout, depthim, points] = getMaskFromCAD(P, mesh, [375, 1242]);
%    
    for i = 1:size(image_coord, 1)
        depth(i) = depthim(image_coord(i, 1), image_coord(i, 2));
    end  
    
    image_coord = [image_coord(:, 2), image_coord(:, 1), ones(size(image_coord, 1), 1)];
%     orig_vertices = transformed .* repmat(w, 1, 3);
    orig_vertices = vertices_flat .* repmat(vertices(:, 3), 1, 3);
    orig_vertices = inv(K) * vertices';
    orig_vertices = orig_vertices - repmat(T, 1, size(orig_vertices', 1));
    orig_vertices = inv(sc*(R')) * orig_vertices;
    orig_vertices = orig_vertices';
    
    distance = dist2(image_coord, vertices_flat);
    min_dist = min(distance')';
    
    for i = 1:size(image_coord, 1)
        [row, col] = find(min_dist(i) == distance(i, :));
        index(i) = col(1); 
%         test = vertices(col, 3);
    end

    image_coord = image_coord .* repmat(vertices(index, 3), 1, 3); 
    image_coord = inv(K) * image_coord';
    image_coord = image_coord - repmat(T, 1, size(image_coord', 1));
    image_coord = inv(sc*(R')) * image_coord;
    image_coord = image_coord';
    
    model_coord = image_coord;
    
%     
%     %Get the reverse transform from image to model coords
%     model = mesh;
%     image = mesh;
%     image.vertices = (P * [image.vertices'; ones(1,size(image.vertices,1))])';
%     image.bbox.vertices = (P * [image.bbox.vertices'; ones(1,size(image.bbox.vertices,1))])';
%     caddata.mesh = image;
%     
%     [mesh, ind] = findtransform(caddata, model.bbox.vertices);
%     
%     R = mesh.transform{1}.R;
%     T = mesh.transform{1}.t';
%     sc = mesh.transform{1}.sc;
% 
%     P = K*[sc*(R') T];
% 
%     vertices = (P * [caddata.mesh.vertices' ; ones(size(caddata.mesh.vertices, 1), 1)'])';
%     vertices = vertices ./ repmat(vertices(:, 3), 1, 3);
    
%     for i = 1:size(image_coord, 1)
%         x = points(i, 1);
%         z = -points(i, 2);
%         Y = depth(i);
% 
%         if(Y == inf)
%             model_coord(i, :) = [0, 0, 0];
%         else
%             X = ((x - K(1, 3))*Y)/K(1, 1);
%             Z = ((z - K(2, 3))*Y)/K(2, 2);
%             model_coord(i, :) = [X, Y, Z];
%         end
%     end  

%     model_coord = vertices;

%     [segimout, depthim, points] = getMaskFromCAD(P, mesh, [1242, 375])
end

function [mesh, ind] = findtransform(caddata, vertices)

    vertices_cad = caddata.mesh.bbox.vertices;
    [pointsCAD, ind] = getboxpoints(vertices_cad, caddata.mesh.bbox.faces);
    
    pointsdata = vertices;
    [d, p, transf] = procrustes(pointsdata,pointsCAD);
    transform = struct('t', transf.c(1, :), 'R', transf.T, 'sc', transf.b);
    mesh = caddata.mesh;
    mesh.transform{1} = transform;
    mesh = transform_mesh2(mesh);
    
end

function [vertices, vert_ind] = getboxpoints(vertices, faces)

faces_annot = faces_for_box();
vert_ind = zeros(size(vertices, 1), 1);

for i = 1 : size(vertices, 1)
    [y,x] = find(faces_annot == i);
    y = unique(y);
    
    vert = [1:size(vertices, 1)]';
    for j = 1 : length(y)
        vert = intersect(vert, faces(y(j), :));
    end;
    vert_ind(i) = vert;
end;

vertices = vertices(vert_ind, :);
end

