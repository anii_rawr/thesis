function model_coord = convert_2d_to_3d(image_coord, mesh, P, K, R, T, sc)

    model_coord = zeros(size(image_coord, 1), 3);
    [segimout, depthim, points] = getMaskFromCAD(P, mesh, [375, 1242]);

    vertices = mesh.vertices;
    vertices = P * [vertices';ones(1, size(vertices, 1))];
    vertices = vertices';
    vertices = vertices ./ repmat(vertices(:, 3), 1, 3);
    
%     w = sym('w', [size(vertices, 1), 1]);
%     X = sym('X', [size(vertices, 1), 1]);
%     Y = sym('Y', [size(vertices, 1), 1]);
%     
    x_a = image_coord(:, 2);
    y_a = image_coord(:, 1);
    
    for i = 1:size(x_a, 1)
        Z(i) = depthim(round(y_a(i)), round(x_a(i)));
    end 
    Z_a = Z';
    
    w_a = sym('w', [size(x_a, 1), 1]);
    X_a = sym('X', [size(x_a, 1), 1]);
    Y_a = sym('Y', [size(x_a, 1), 1]);
    
    for i = 1:size(x_a, 1)
        w = w_a(i);
        x = x_a(i);
        y = y_a(i);
        Y = Y_a(i);
        X = X_a(i);
        Z = Z_a(i);
        q = [w'.*x'; w'.*y'; w'];
        Q = [X'; Y'; Z'; 1];
        a = q(1) == (P(1, :) * Q);
        b = q(2) == (P(2, :) * Q);
        c = q(3) == (P(3, :) * Q);
%         a = w * X^2 + Y^2 ==0;
%         b = X - Y == 1;
%         c = w^2 + 6 == 5*w;
        [w_a(i), X_a(i), Y_a(i)] = solve(a, b, c, w, X, Y);
    end
    
    w = double(w_a);
    X = double(X_a);
    Y = double(Y_a);
    Z = Z_a;
    X = changem(X, [NaN], [Inf]);
    Y = changem(Y, [NaN], [Inf]);
    Z = changem(Z, [NaN], [Inf]);
    
    model_coord = [X Y Z];
    
%     Q = [X'; Y'; Z; ones(1, size(x, 1))];
%     q = [w'.*x'; w'.*y'; w'];
        
%     X_A = (P(4)-P(6)*x)./(x*P(3) - P(1));
%     X_B = (P(7)*Z + P(10) - P(12)*x - P(9)*Z.*x)./(x*P(3)-P(1));
%     Y = (P(1)*X_B + P(7)*Z + P(10) - y*P(3).*X_B - P(9)*Z.*y - P(12)*y)./(X_A.*y*P(3) + P(6)*y - P(1)*X_A - P(5));
%     X = Y.*X_A + X_B;
%     w = P(3)*X + P(6)*Y + P(9)*Z + P(12);
end