function[R, T] = compute3DAligningTransform(Ms, Ps, o)
% Given a set of scene points 'Ps', their corresponding model points 'Ms',
% and the origin 'o' of the model coordinate frame, estimates the rotation
% and translation between both sets of points.
%
% Based on K. S. Arun, T. S. Huang, and S. D. Blostein. Least square
% fitting of two 3-d point sets. IEEE Transactions on Pattern Analysis and
% Machine Intelligence, 9(5):698 -- 700, 1987.
%
% for CSC 420
% by Pablo Sala, University of Toronto, September 2006

% Express the model and scene points with the origin in their centroid:
Ms2 = Ms - repmat(mean(Ms,2), 1, size(Ms,2));
Ps2 = Ps - repmat(mean(Ps,2), 1, size(Ps,2));

% Compute the rotation R centered at Ms(:,1):
    % Generate a matrix H = sum_i (Ms2_i * Ps2_i'):
    H = Ms2 * Ps2';

    % Compute the orthonormal matrix closest to R (in the sense of the Frobenius norm):
    [U,S,V] = svd(H);

    if(det(U)*det(V) > 0)
        d3 = +1;
    else
        d3 = -1;
    end

    R = V * diag([1, 1, d3]) * U';

% Compute the translation T, taking into account that the model coordinate
% frame has the origin in 'o':
T = mean(Ps,2) - mean(R*(Ms-repmat(o,1,size(Ms,2))),2);
