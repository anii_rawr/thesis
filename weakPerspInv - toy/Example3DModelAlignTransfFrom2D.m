% Example of Computation of a 3D Model Aligning Transformation from a 2D Image
% for CSC 420
%
% by Pablo Sala, University of Toronto, May 2006

% Clear variables, command window and close all figures:
clc; clear; close all;

% Read and show input image:
im = imread('cube100.jpg');
figure; image(im); hold on;

% These are the selected image points (corresponding to the visible corners of the box):
ps = [695.1 221.1;
      706   503.2;
      998.8 544.4;
      825.6 640.6;
      576.2 611.7;
      574.6 363.8]';

% Plot the image points and lines in blue:
Xs = [ps(1,1:6),          ps(1,2);
      ps(1,2:6), ps(1,1), ps(1,5)];

Ys = [ps(2,1:6),          ps(2,2);
      ps(2,2:6), ps(2,1), ps(2,5)];

plot(ps(1,:),ps(2,:),'ob');
line(Xs, Ys, 'Color', 'b', 'LineWidth', 2);

% These are the model points. The first six model points correspond to the image points listed above:
% a = 77.4474; b = 75.7396; c = 83;
% a = 77.4474; b = 83; c = 75.7396;
% a = 75.7396; b = 77.4474; c = 83;
% a = 75.7396; b = 83; c = 77.4474;
% a = 83; b = 75.7396; c = 77.4474;
a = 83; b = 77.4474; c = 75.7396;

Ms = [0   a  b;
    0   a  0;
    c   a  0;
    c   0  0;
    0 0 0;
    0   0  b;
    c   0  b;
    c   a  b]';

% Move the origin of the model points to their centroid:
Ms = Ms - repmat(mean(Ms,2),1,size(Ms,2));

% Find the scaling factor and 3D coordinates of the scene points using the
% first three image and corresponding model points:
[P0, P1, P2, s] = weakPerspInv(Ms(:,1), Ms(:,2), Ms(:,3), ps(:,1), ps(:,2), ps(:,3));

% Determine the translation and rotation that the first three model points
% have to undergo in order to match the three scene points, given that the
% center of rotation for the model is in the origin:
M0 = Ms(:,1); M1 = Ms(:,2); M2 = Ms(:,3);
[R, T] = compute3DAligningTransform([M0, M1, M2], [P0, P1, P2], [0;0;0]);

% Backproject the model:
    % Rotate, translate, and scale the model:
    Qs = s*(R*Ms + repmat(T,1,size(Ms,2)));

    % Create a list with the coordinates of the transformed model points:
    Xs = [Qs(1,1:8),        Qs(1,2),Qs(1,1),Qs(1,4),Qs(1,3);
          Qs(1,2:8),Qs(1,1),Qs(1,5),Qs(1,6),Qs(1,7),Qs(1,8)];

    Ys = [Qs(2,1:8),        Qs(2,2),Qs(2,1),Qs(2,4),Qs(2,3);
          Qs(2,2:8),Qs(2,1),Qs(2,5),Qs(2,6),Qs(2,7),Qs(2,8)];

    Zs = [Qs(3,1:8),        Qs(3,2),Qs(3,1),Qs(3,4),Qs(3,3);
          Qs(3,2:8),Qs(3,1),Qs(3,5),Qs(3,6),Qs(3,7),Qs(3,8)];

    % Display the backprojected model:
    figure; h = image(im); hold on;
    plot3(Qs(1,:), Qs(2,:), Qs(3,:), 'or'); text(Qs(1,:), Qs(2,:), Qs(3,:), num2str((1:size(Qs,2))'), 'Color', 'w', 'LineWidth', 2);
    line(Xs, Ys, Zs, 'Color', 'r', 'LineWidth', 2);
    set(h,'AlphaData',ones(size(im,1),size(im,2)) * 0.90);
