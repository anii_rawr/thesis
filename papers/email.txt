Hi Annie,

I've run a potential project by Sanja and Raquel (two other vision faculty members) that I think will be appropriate for your 4th year thesis.  I've also asked Paul Gries, our Assoc. Chair, to find a way for you to take Sanja's course.

I think you'll like the project. It's on the problem of 3-D object recognition from 2-D images.  Don't worry -- you won't need to understand all of computer vision to do the project, and we'll help you focus on what you need to know.  Some things ot get started with (if you'd like to start early):

1. You'll be working with 3-D CAD models of objects. Sanja and a summer student of ours (Jialiang Wang) are working on another project which involves manipulating CAD models.  If you contact Jialiang (jialiang.wang@mail.utoronto.ca), he could show you the tools and the data to manipulate the models.  

2. If you look at ant intro vision text book, e.g., Trucco and Verri, you should read the section(s) on camera geometry and projection, so that you understand how points on a 3-D model get projected into an image.  Pay particular attention to the weak perspective projection camera model.

3. You should read the paper on SIFT: http://www.cs.ubc.ca/~lowe/papers/ijcv04.pdf
Then go to http://www.cs.ubc.ca/~lowe/keypoints/ and you can download the code for SIFT (many versions exist, and Sanja may have a pointer to a better version).

4. Have a look at the attached pdf, which are notes from an old class I taught.  Start on page 103 (3-D from Weak perspective). I think it's covered in Trucco and Verri (the text I used for the course).  The reference for this method is attached (T.D. Alter).  My student, Pablo Sala (psala@cs.toronto.edu) was my TA for the course and implemented the code (in Matlab, I think). Please cptact him and ask him for a copy of the code.

So, here's the idea, Annie.  Given that you've read my chapter, you might be able to get the gist of what I'm proposing.

1. Say you have a set of images taken around a real 3-D object in some controlled manner, e.g., one view every 10 degrees. Let's also assume that for each of these views, you can align a CAD model with the view, i.e., if you overlay the CAD model on top of the image, so that it aligns with what you see, you're effectively specifying the position and orientation of the CAD model in the world such that when you project it through the camera, it aligns with the actual image of the object.

2. For each actual image, you compute SIFT features that give you 2-D points (and a high-dimensional descriptor for each point).  If you know the 2-D points in the image, and you know the position and orientation of the 3-D CAD model with respect to the image, you can compute exactly where on the 3-D CAD model the 2-D point corresponds to.  Think of these points as distinctive 2-D markings you detect on the actual object you're imaging, and this process figures out the 3-D points on the CAD model that those points actually correspond to (you effectively draw a ray from the camera center through the 2-D image point and figure out where it intersects the 3-D surface of the CAD model).  Think of this process as a "training" process which "learns" the salient image features that don't originally live on the CAD model, and maps them to 3-D points on the CAD model.  Or, in other words, you're actually making the CAD model more realistic in that not only does it capture the geometry of the object, nut now it captures some of the more important "appearance" of the object.  You repeat this process for each of the views of the object, and what you get in the end is a mapping from 2-D SIFT features to 3-D model features.

3. Ok, now let's move to "runtime". You take a picture of the real object (that you trained on) in a different, i.e., real, setting, and compute the SIFT features once again. A simple process can tell you, for a given SIFT feature, which 3-D points on which 3-D models you might be looking at (this is effectively a look-up table implemented as a nearest-neighbor search).  Once you have 3 such points (all of which map to different 3-D points on the same 3-D object, you use the Alter algorithm to compute the position and orientation of the CAD model with respect to the image, yielding the exact pose and identity of the object.  You're done!

This will all take a lot of time to digest, Annie, and I don't want you to get overwhelmed right now.  There is code available for almost every single component of what I talked about, so you won't get too mired in the details. But the more you understand what you're doing, the more satisfying the experience will be.  The first thing to do is to understand the "big picture" here, what we're doing and why we're doping this.  I'm happy to meet with you next week and go over the big picture, to help motivate you and provide the necessary context.  You don't have to understand all the details in these papers, but you do need to understand what they're trying to do, what assumptions they're making and why, and what's happening at a high level.  It's all linear algebra and calculus in the end -- nothing you haven't seen before. But it can be a bit intimidating at times.

We can start meeting whenever you want, or can meet by email. The more you get done over the summer, the more progress you'll make next year.  I'm not sure what your summer plans are, but if you're not busy, I can have you join my group as a summer student, and I'd pay you a small stipend ($1500) over the summer, which is what my other volunteer research interns get.

I know this is a lot to digest, Annie. As I said, I'm happy to meet.  I've offered to both Sanja and Raquel the opportunity to get involved if they like, as collaborators on the idea, so you may get the opportunity to interact with 3 faculty and some of our grad students.

Cheers,

Sven