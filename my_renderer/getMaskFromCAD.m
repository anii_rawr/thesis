function [segimout, depthim, points] = getMaskFromCAD(K, mesh, imsize)

%[x, y] = project_points(K, mesh.vertices);
pp = projectToImage(mesh.vertices',K);
pp = pp';
x = pp(:, 1); y = pp(:, 2);
faces = double(mesh.faces);

segimout = zeros(imsize(1:2));
depthim = inf * ones(imsize(1:2));
[X,Y] = meshgrid(1:imsize(2), 1:imsize(1));

%Z = mxgridtrimesh(faces,[x,y,zeros(size(x,1),1)],X,Y);
Z = mxgridtrimesh(faces,[x,y,mesh.vertices(:, 3)],X,Y);
ind = find(~isnan(Z));
segimout(ind) = 1;
depthim(ind) = Z(ind);
points = [x, y];