function im_render = project_CAD(imname, cad_id)

if nargin < 2
    imname = '000010';
    cad_id = 1;
end;

im = imread([imname '.png']);
calib_file = [imname '.txt'];
box_file = [imname '.mat'];

MODEL_DIR = pwd;
data = load(fullfile(MODEL_DIR, sprintf('car_%03d_mesh.mat', cad_id)));
[~,~,calib] = loadCalibration(calib_file);
caddata.mesh = data.mesh;


[K, R, t] = art(calib.P_rect{3});
figure('position', [5,5,size(im,2), size(im, 1)]);
subplot('position',[0,0,1,1]);
imshow(im);
hold on;
data = load(box_file);
box3D = data.annotation.box3D{1}; % just take first box;

   vertices = box3D.vertices;
   boxView = project_points(vertices, K);
   [mesh, ind] = findtransform(caddata, vertices);
   v = getfacedist(mesh);
   mesh.faces = mesh.faces(v, :);
   %close all;
   vertices = project_points(mesh.vertices, K);
   h=renderimage([], mesh.faces, vertices, mesh, 1);
   M = getframe;
   im_render = M.cdata;
   hold on;
   plot_boxView([], boxView');
end

function vertices = project_points(vertices, K)
   vertices = (K * vertices')';
   vertices = vertices ./ repmat(vertices(:, 3), [1,3]);
end


function [x_out,y_out] = generate_traj(x,y)

n = 10;
x_out = [];
y_out = [];
for i = 1 : length(x)-1
    x_i = x(i) + [0:1/n:1-1/n]'*(x(i+1)-x(i));
    y_i = y(i) + [0:1/n:1-1/n]'*(y(i+1)-y(i));
    x_out = [x_out; x_i];
    y_out = [y_out; y_i];
end;

end


function vertices = generate_box3D(p, dir3D, gr_plane, dims)

dir3D = dir3D/norm(dir3D);
ng = gr_plane(1:3)/norm(gr_plane(1:3));
nf = cross(dir3D, ng);
nf = nf/norm(nf);
vertices = zeros(8, 3);
vertices(6, :) = (p - nf*dims(1)/2)';
vertices(5, :) = (p + nf*dims(1)/2)';
vertices(7, :) = (vertices(6,:)' + ng*dims(2))';
vertices(8, :) = (vertices(5,:)' + ng*dims(2))';
vertices(1, :) = (vertices(5,:)' + dir3D*dims(3))';
vertices(2, :) = (vertices(6,:)' + dir3D*dims(3))';
vertices(3, :) = (vertices(2,:)' + ng*dims(2))';
vertices(4, :) = (vertices(1,:)' + ng*dims(2))';

end

function [vertices, vert_ind] = getboxpoints(vertices, faces)

faces_annot = faces_for_box();
vert_ind = zeros(size(vertices, 1), 1);

for i = 1 : size(vertices, 1)
    [y,x] = find(faces_annot == i);
    y = unique(y);
    
    vert = [1:size(vertices, 1)]';
    for j = 1 : length(y)
        vert = intersect(vert, faces(y(j), :));
    end;
    vert_ind(i) = vert;
end;

vertices = vertices(vert_ind, :);
end

function v = getfacedist(mesh)

vertices = mesh.vertices;
faces = mesh.faces;
d = sqrt(vertices(:, 1).^2 + vertices(:, 2).^2 + vertices(:, 3).^2);
ind = find(faces);
dfaces = zeros(size(faces));
dfaces(ind) = d(faces(ind));
dfaces = max(dfaces, [], 2);
%dfaces = mean(dfaces, 2);
[u, v] = sort(dfaces, 'descend');

end



function [mesh, ind] = findtransform(caddata, vertices)

    vertices_cad = caddata.mesh.bbox.vertices;
    [pointsCAD, ind] = getboxpoints(vertices_cad, caddata.mesh.bbox.faces);
    
    pointsdata = vertices;
    [d, p, transf] = procrustes(pointsdata,pointsCAD);
    transform = struct('t', transf.c(1, :), 'R', transf.T, 'sc', transf.b);
    %transform.t = point3D;
    %transform.sc = 1;
    %transform.R = pinv(pointsCAD(2:end,:)') * pointsdata(2:end,:)';
    mesh = caddata.mesh;
    mesh.transform{1} = transform;
    mesh = transform_mesh2(mesh);
    
end


function h=renderimage(im, faces, vertices, meshtransf, rendertexture)

if ~isempty(im)
figure('position', [5,5,size(im,2), size(im, 1)]);
subplot('position',[0,0,1,1]);
imshow(im);
hold on;
end;
if ~rendertexture
   h=trisurf(faces,vertices(:,1),vertices(:,2),ones(size(vertices, 1), 1),'EdgeColor','none','FaceColor',[1,1,1]);
else
   h=trisurf(faces,vertices(:,1),vertices(:,2),ones(size(vertices, 1), 1),'facevertexcdata',meshtransf.colors,'EdgeColor','none');
end;
end