function [P0, P1, P2, s] = weakPerspInv(M0, M1, M2, p0, p1, p2)
% Given three 2D image points p0, p1, p2, and their corresponding 3D model
% points M0, M1, M2, this function returns the 3D coordinates of the scene points and the
% scaling factor s.
%
% Based on "3D Pose from 3 Corresponding Points under Weak-Perspective
% Projection", by T. D. Alter, MIT A.I. Memo No. 1378, July 1992. This is
% the algorithmn described in "Introductory Techniques for 3-D computer
% vision" by Trucco & Verri, section 11.2.2
%
% for CSC 420
% by Pablo Sala, University of Toronto, May 2006

% Distances between model points:
D01 = norm(M0 - M1); D02 = norm(M0 - M2); D12 = norm(M1 - M2);

% Distances between image points:
d01 = norm(p0 - p1); d02 = norm(p0 - p2); d12 = norm(p1 - p2);

Ds = [D01; D02; D12];
ds = [d01; d02; d12];

A1 = [-1  1  1;
       1 -1  1;
       1  1 -1];

A2 = [ 1  1  1;
          A1  ];

a = prod(A2 * Ds);
b = Ds'.^2 * A1 * ds.^2;
c = prod(A2 * ds);

% Compute scaling factor:
s = sqrt((b + sqrt(b^2-a*c))/a);

if([1  1 -1] * ds.^2 <= s^2 * [1  1 -1] * Ds.^2)
    sigma = 1;
else
    sigma = -1;
end

h1 = sqrt((s*D01)^2 - d01^2);
h2 = sigma*sqrt((s*D02)^2 - d02^2);

% Compute the depths of the scene points:
P0 = 1/s * [p0;0];
P1 = 1/s * [p1;h1];
P2 = 1/s * [p2;h2];
