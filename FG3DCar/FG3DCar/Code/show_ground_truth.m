clear all;
close all;
clc;

globals;

% load face, edge, landmark
[face,edge,landmark] = load_model();

% load ground truth
load('../3DModel/ground_truth','manualParam');

% get image list
images = dir(fullfile('../dataset/original/*.jpg'));
images = cellfun(@(x) x,{images.name},'UniformOutput',false);

% loop over each image
for i=1:length(images)
    
    % find corresponding index in ground truth file
    index = find(cellfun(@(x) strcmp(x.filename,images(i)),manualParam));
    
    % load image
    filename = manualParam{index}.filename;
    img = imread(fullfile('../dataset/original/',filename));
    
    % load 2D points
    points(:,1:2) = manualParam{index}.bestModel(:,1:2);
    
    % load visible faces
    visible_faces = manualParam{index}.visibleFace;
    
    % visible points
    visible_point = cal_visible_point(visible_faces,face,256);
   
    % draw
    close all;
    imshow(img); hold on;
    
    visualize_wireframe(points,visible_faces,face);
    visualize_edge(points,visible_point,edge);
    visualize_landmark(points,visible_point,landmark,'y');
    pause;
end

