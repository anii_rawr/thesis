%% In this demo, we fit a 3D deformable model to a 2D image 
%% given the initial pose and ground truth landmark locations. 
function model_fitting()

clear all;
close all;
clc;

globals;

%%-------------------------------------------------------------------------
%% Parameters
%%-------------------------------------------------------------------------
param = create_configuration();
param.iteration_num = 60;       % iterations for Jacobian
param.alpha = 0.1;              % step size for Jacobian 
param.prior = 'mean';           % mean proir as initial shape parameters

%%-------------------------------------------------------------------------
%% Load deformable 3D car model
%%-------------------------------------------------------------------------
G = load('../3dmodel/eigen_car_mesh.mat','eigen_car_mesh');     
sigma = load('../3dmodel/eigen_car_mesh.mat','eigen_car_value');  
U = load('../3dmodel/mean_car_mesh.mat','avgModel_mesh');         
G = G.eigen_car_mesh;               % D-by-n,eigen-car
sigma = diag(sigma.eigen_car_value);% n-by-n (diagnoal matrix),eigen-value
U = U.avgModel_mesh;                % D-by-1,mean-car

%%-------------------------------------------------------------------------
%% Load face, edge and landmark
%%-------------------------------------------------------------------------
[face,edge,landmark] = load_model();

%%-------------------------------------------------------------------------
%% Load ground truth
%%-------------------------------------------------------------------------
load('../3dmodel/ground_truth.mat','manualParam');
gt_param = manualParam;

%%-------------------------------------------------------------------------
%% Load initial pose
%%-------------------------------------------------------------------------
load('../3dmodel/initial_pose.mat','manualParam');
init_param = manualParam;

%%-------------------------------------------------------------------------
%% Get image list
%%-------------------------------------------------------------------------
images = dir(fullfile('../dataset/original/*.jpg'));
images = cellfun(@(x) x,{images.name},'UniformOutput',false);
imagedir = '../dataset/original/';

%% start 
for i=25:length(images)
    
    disp(['processing images:' num2str(i)]);
    index = find(cellfun(@(x) strcmp(x.filename,images(i)),manualParam));
    
    %% Load image
    filename = manualParam{index}.filename;
    img = imread(fullfile(imagedir,filename));
    
    %% Initial pose  
 	f           = 2.7;    
    tx          = init_param{index}.bestParam(3);
    ty          = init_param{index}.bestParam(4);
    tz          = init_param{index}.bestParam(5);
    rx          = init_param{index}.bestParam(6);
    ry          = init_param{index}.bestParam(7);
    rz          = init_param{index}.bestParam(8);
    u0          = init_param{index}.bestParam(9);
    v0          = init_param{index}.bestParam(10);
    scale       = init_param{index}.bestParam(11);
    gt_shape    = gt_param{index}.bestParam(12:end)';
    
    %% 2D image landmark correspondences
    %% Here, we use ground truth image landmarks for the demostration
    %% It can be simply replaced by regression models or other landmark detection 
    %% algorithms for localizing 2D landmark locations,
    %% which will be used as inputs for 3D model fitting
    gt_points(:,1:2) = gt_param{index}.bestModel(:,1:2);
    
   
    %% Shape prior
    %% Here, we use mean shape or ground truth shape for the demonstration. 
    %% Shape prior can be estimated by leveraging class label information
    %% That is, learning a mapping function from class label to shape parameters
    %% and then using the predict class label (estimated by classification results)
    %% to generate a better shape prior  
    
    switch(param.prior)
        case 'mean',   shape_prior = zeros(10,1);
        case 'model',  shape_prior = gt_shape;
    end
    
    
    %%---------------------------------------------------------------------
    %% Generate initial pose 
    %%--------------------------------------------------------------------- 

    %% Get initial pose ans shape
    shape = shape_prior;
    X_wd = reconstruct(G,U,shape);
    
    %% projection
    R   = projection('get_rotation_matrix',rx,ry,rz,2);
    t   = projection('get_translation_matrix',tx,ty,tz);
    Rt  = projection('get_extrinsic_matrix',R,t);
    K   = projection('get_intrinsic_matrix',f,scale,scale,u0,v0);
    
    X_cam = projection('world2camera',X_wd,Rt);
    x_img = projection('camera2image',X_cam,K);
    x_img = x_img';
    points(:,1:2) = x_img(:,1:2);
    
    %% face normal
    normal = compute_normal(face,X_wd);
    normal = R * normal;
    
    %% visible face
    normal = normal';
    visible_faces = find(normal(:,3) > 0);
    
    %% visible points
    visibility = cal_visible_point(visible_faces,face,256);  
    
    %%---------------------------------------------------------------------
    %% jacobian system 
    %%---------------------------------------------------------------------
    n = size(G,2); %number of eigen-car
    
    % projection matrix
    P = zeros(3,4);
    P(:,1:3) = K;
  
    %% initialize
    p = initialize(shape,R,t,n); 
    iteration = 0; 
    min = inf;
   
    while 1
         iteration = iteration + 1;
         J = []; e = [];
         for j=1:length(landmark)
             
            % landmark index
            k = landmark(j);
            
            % corresponding points
            u = points(k,:);
            v = gt_points(k,:);
            
            % find matched boundary
            index = find(edge(:,1) == k | ...
                         edge(:,2) == k,1);
              
            % get end points of the boundary
            vi = edge(index,1);
            vj = edge(index,2);
            ua = points(vi,:);
            ub = points(vj,:);
            
            % computer jacobian ji 
            if iteration <= 30
               [ji,ei] = compute_jacobian(u,v,ua,ub,k,P,G,U,p,'pose');
            else
               [ji,ei] = compute_jacobian(u,v,ua,ub,k,P,G,U,p,'shape');
            end
            J = [J;ji]; %#ok<AGROW>
            e = [e;ei]; %#ok<AGROW>
         end
         
         %% stopping criteria
         error = sum(abs(e))/length(e);
         if error < min,min = error; end
         disp(['iteration =' num2str(iteration) ',' 'error =' num2str(error)]);
         
         if iteration >= param.iteration_num || ... 
		    (error > min +0.2 && ...
		    iteration ~= 2)
            
            %% return 
            p = p-dp*param.alpha; 
            
            %% update parameters
            [shape,R,t] = update(p,n);
            
            %% reconstruction
            X_wd = reconstruct(G,U,shape);
            
            %% projection
            Rt = projection('get_extrinsic_matrix',R,t);
            X_cam = projection('world2camera',X_wd,Rt);
            x_img = projection('camera2image',X_cam,K);
            x_img = x_img';
            points(:,1:2) = x_img(:,1:2);
            
            %% face normal
            normal = compute_normal(face,X_wd);
            normal = R * normal;
            
            %% visible face
            normal = normal';
            visible_faces = find(normal(:,3) > 0);
       
            %% visible points
            point_num = size(G,1)/3;
            visibility = cal_visible_point(visible_faces,face,point_num);
  
		    break;
         end
         
         %% solve Jacobian system
         dp = pinv(J'*J)*J'*e;
         p = p+dp*param.alpha;
         
         %% update parameters
         [shape,R,t] = update(p,n);

         %% reconstruction
         X_wd = reconstruct(G,U,shape);
         Rt = projection('get_extrinsic_matrix',R,t);
         
         %% projection
         X_cam = projection('world2camera',X_wd,Rt);
         x_img = projection('camera2image',X_cam,K);
         x_img = x_img';
         points(:,1:2) = x_img(:,1:2);
         
         %% face normal
         normal = compute_normal(face,X_wd);
         normal = R * normal;
         
         %% visible face
         normal = normal';
         visible_faces = find(normal(:,3) > 0);
       
         %% visible points
         point_num = size(G,1)/3;
         visibility = cal_visible_point(visible_faces,face,point_num);
                 
         %% show
         imshow(img); hold on;
         visualize_edge(points,visibility,edge);
         visualize_correspondence(points(landmark,:),gt_points(landmark,:),landmark,'y','g','c');
         drawnow;
    end 
   
    %% show
    close all;
    imshow(img); hold on;
    visualize_edge(points,visibility,edge);
    visualize_correspondence(points(landmark,:),gt_points(landmark,:),landmark,'y','g','c');
    pause;
end

