function phow_caltech101()
% PHOW_CALTECH101 Image classification in the Caltech-101 dataset
%   This program demonstrates how to use VLFeat to construct an image
%   classifier on the Caltech-101 data. The classifier uses PHOW
%   features (dense SIFT), spatial histograms of visual words, and a
%   Chi2 SVM. To speedup computation it uses VLFeat fast dense SIFT,
%   kd-trees, and homogeneous kernel map. The program also
%   demonstrates VLFeat PEGASOS SVM solver, although for this small
%   dataset other solvers such as LIBLINEAR can be more efficient.
%
%   By default 15 training images are used, which should result in
%   about 64% performance (a good performance considering that only a
%   single feature type is being used).
%
%   Call PHOW_CALTECH101 to train and test a classifier on a small
%   subset of the Caltech-101 data. Note that the program
%   automatically downloads a copy of the Caltech-101 data from the
%   Internet if it cannot find a local copy.
%
%   Edit the PHOW_CALTECH101 file to change the program configuration.
%
%   To run on the entire dataset change CONF.TINYPROBLEM to FALSE.
%
%   The Caltech-101 data is saved into CONF.CALDIR, which defaults to
%   'data/caltech-101'. Change this path to the desired location, for
%   instance to point to an existing copy of the Caltech-101 data.
%
%   The program can also be used to train a model on custom data by
%   pointing CONF.CALDIR to it. Just create a subdirectory for each
%   class and put the training images there. Make sure to adjust
%   CONF.NUMTRAIN accordingly.
%
%   Intermediate files are stored in the directory CONF.DATADIR. All
%   such files begin with the prefix CONF.PREFIX, which can be changed
%   to test different parameter settings without overriding previous
%   results.
%
%   The program saves the trained model in
%   <CONF.DATADIR>/<CONF.PREFIX>-model.mat. This model can be used to
%   test novel images independently of the Caltech data.
%
%     load('data/baseline-model.mat') ; # change to the model path
%     label = model.classify(model, im) ;
%

% Author: Andrea Vedaldi

% Copyright (C) 2011-2013 Andrea Vedaldi
% All rights reserved.
%
% This file is part of the VLFeat library and is made available under
% the terms of the BSD license (see the COPYING file).

% modification by Yenliang, 2013-09-01
run ../toolbox/vl_setup;
addpath('../Liblinear/matlab');
conf.tr_dir = 'data/BMVC/train';
conf.ts_dir = 'data/BMVC/test';
conf.dataDir = 'data';

%conf.autoDownloadData = true ;
%conf.numTrain = 15;
%conf.numTest = 15;

conf.numClasses = 14;
conf.numWords = 2048;
conf.numSpatialX = [1 2 4] ;
conf.numSpatialY = [1 2 4] ;
conf.quantizer = 'kdtree' ;
conf.svm.C = 10;

%conf.svm.solver = 'sdca' ;
%conf.svm.solver = 'sgd' ;
conf.svm.solver = 'liblinear' ;

conf.svm.biasMultiplier = 1 ;
conf.phowOpts = {'Step', 3} ;
conf.clobber = false ;
conf.tinyProblem = false ;
conf.prefix = 'baseline' ;
conf.randSeed = 1;

if conf.tinyProblem
  conf.prefix = 'tiny' ;
  conf.numClasses = 5 ;
  conf.numSpatialX = 2 ;
  conf.numSpatialY = 2 ;
  conf.numWords = 300 ;
  conf.phowOpts = {'Verbose', 2, 'Sizes', 7, 'Step', 5} ;
end

conf.vocabPath = fullfile(conf.dataDir, [conf.prefix '-vocab.mat']) ;
conf.trhistPath = fullfile(conf.dataDir, [conf.prefix '-trhists.mat']) ;
conf.tshistPath = fullfile(conf.dataDir, [conf.prefix '-tshists.mat']) ;
conf.modelPath = fullfile(conf.dataDir, [conf.prefix '-model.mat']) ;
conf.resultPath = fullfile(conf.dataDir, [conf.prefix '-result']) ;

randn('state',conf.randSeed) ;
rand('state',conf.randSeed) ;
vl_twister('state',conf.randSeed) ;

% --------------------------------------------------------------------
%                                            Download Caltech-101 data
% --------------------------------------------------------------------

% if ~exist(conf.calDir, 'dir') || ...
%    (~exist(fullfile(conf.calDir, 'airplanes'),'dir') && ...
%     ~exist(fullfile(conf.calDir, '101_ObjectCategories', 'airplanes')))
%   if ~conf.autoDownloadData
%     error(...
%       ['Caltech-101 data not found. ' ...
%        'Set conf.autoDownloadData=true to download the required data.']) ;
%   end
%   vl_xmkdir(conf.calDir) ;
%   calUrl = ['http://www.vision.caltech.edu/Image_Datasets/' ...
%     'Caltech101/101_ObjectCategories.tar.gz'] ;
%   fprintf('Downloading Caltech-101 data to ''%s''. This will take a while.', conf.calDir) ;
%   untar(calUrl, conf.calDir) ;
% end
% 
% if ~exist(fullfile(conf.calDir, 'airplanes'),'dir')
%   conf.calDir = fullfile(conf.calDir, '101_ObjectCategories') ;
% end

% --------------------------------------------------------------------
%                                                           Setup data
% --------------------------------------------------------------------
classes = dir(conf.tr_dir);
classes = classes([classes.isdir]);
classes = {classes(3:conf.numClasses+2).name} ;

tr_images = {};
ts_images = {};
tr_imageClass = {};
ts_imageClass = {};

for ci = 1:length(classes)
  tr_ims = dir(fullfile(conf.tr_dir, classes{ci}, '*.jpg'));
  ts_ims = dir(fullfile(conf.ts_dir, classes{ci}, '*.jpg'));
 
  tr_ims = cellfun(@(x)fullfile(classes{ci},x),{tr_ims.name},'UniformOutput',false);
  ts_ims = cellfun(@(x)fullfile(classes{ci},x),{ts_ims.name},'UniformOutput',false);
  
  tr_images = {tr_images{:}, tr_ims{:}};
  ts_images = {ts_images{:}, ts_ims{:}};
  
  tr_imageClass{end+1} = ci * ones(1,length(tr_ims));
  ts_imageClass{end+1} = ci * ones(1,length(ts_ims));
  fprintf('number of training examples %d\n', length(tr_ims)); 
  fprintf('number of testing examples %d\n', length(ts_ims)); 
    
end

% selTrain = find(mod(0:length(images)-1, conf.numTrain+conf.numTest) < conf.numTrain) ;
% selTest = setdiff(1:length(images), selTrain) ;
tr_imageClass = cat(2, tr_imageClass{:});
ts_imageClass = cat(2, ts_imageClass{:});

model.classes = classes ;
model.phowOpts = conf.phowOpts ;
model.numSpatialX = conf.numSpatialX ;
model.numSpatialY = conf.numSpatialY ;
model.quantizer = conf.quantizer ;
model.vocab = [] ;
model.w = [] ;
model.b = [] ;
model.classify = @classify ;

% --------------------------------------------------------------------
%                                                     Train vocabulary
% --------------------------------------------------------------------

if ~exist(conf.vocabPath) || conf.clobber

  % Get some PHOW descriptors to train the dictionary
  tr_images_sub = vl_colsubset(tr_images, 30) ;
  descrs = {} ;
  %for ii = 1:length(selTrainFeats)
  parfor ii = 1:length(tr_images_sub)
    im = imread(fullfile(conf.tr_dir, tr_images_sub{ii})) ;
    im = standarizeImage(im) ;
    [drop, descrs{ii}] = vl_phow(im, model.phowOpts{:}) ;
  end
  
  descrs = vl_colsubset(cat(2, descrs{:}), 10e4) ;
  descrs = single(descrs);

  % Quantize the descriptors to get the visual words
  vocab = vl_kmeans(descrs, conf.numWords, 'verbose', 'algorithm', 'elkan', 'MaxNumIterations', 50) ;
  save(conf.vocabPath, 'vocab') ;
else
  load(conf.vocabPath) ;
end

model.vocab = vocab ;

if strcmp(model.quantizer, 'kdtree')
  model.kdtree = vl_kdtreebuild(vocab) ;
end

% --------------------------------------------------------------------
%                                           Compute spatial histograms
% --------------------------------------------------------------------
% training
if ~exist(conf.trhistPath) || conf.clobber
  tr_hists = {};
  %%parfor ii = 1:length(tr_images)
  for ii = 1:length(tr_images)
    fprintf('Processing %s (%.2f %%)\n', tr_images{ii}, 100 * ii / length(tr_images)) ;
    im = imread(fullfile(conf.tr_dir, tr_images{ii})) ;
    tr_hists{ii} = getImageDescriptor(model, im);
  end

  tr_hists = cat(2, tr_hists{:}) ;
  save(conf.trhistPath, 'tr_hists') ;
else
  load(conf.trhistPath) ;
end

% testing
if ~exist(conf.tshistPath) || conf.clobber
  ts_hists = {};
  parfor ii = 1:length(ts_images)
  % for ii = 1:length(images)
    fprintf('Processing %s (%.2f %%)\n', ts_images{ii}, 100 * ii / length(ts_images)) ;
    im = imread(fullfile(conf.ts_dir, ts_images{ii})) ;
    ts_hists{ii} = getImageDescriptor(model, im);
  end

  ts_hists = cat(2, ts_hists{:}) ;
  save(conf.tshistPath, 'ts_hists') ;
else
  load(conf.tshistPath) ;
end

% --------------------------------------------------------------------
%                                                  Compute feature map
% --------------------------------------------------------------------

tr_psix = tr_hists;
ts_psix = ts_hists;
%tr_psix = vl_homkermap(tr_hists, 1, 'kchi2', 'gamma', .5);
%ts_psix = vl_homkermap(ts_hists, 1, 'kchi2', 'gamma', .5);

% --------------------------------------------------------------------
%                                                            Train SVM
% --------------------------------------------------------------------

if ~exist(conf.modelPath) || conf.clobber
  switch conf.svm.solver
    case {'sgd', 'sdca'}
      lambda = 1 / (conf.svm.C *  length(tr_images)) ;
      w = [] ;
      for ci = 1:length(classes)
        perm = randperm(length(tr_images)) ;
        fprintf('Training model for class %s\n', classes{ci}) ;
        y = 2 * (tr_imageClass(:) == ci) - 1 ;
        [w(:,ci) b(ci) info] = vl_svmtrain(tr_psix(:, perm), y(perm), lambda, ...
          'Solver', conf.svm.solver, ...
          'MaxNumIterations', 50/lambda, ...
          'BiasMultiplier', conf.svm.biasMultiplier, ...
          'Epsilon', 1e-3);
      end

    case 'liblinear'
      svm = train(tr_imageClass', ...
                  sparse(double(tr_psix)),  ...
                  sprintf(' -s 0 -B %f -c %f', ...
                  conf.svm.biasMultiplier, conf.svm.C), ...
                  'col') ;


      w = svm.w(:,1:end-1)' ;
      b =  svm.w(:,end)' ;
  end

  model.b = conf.svm.biasMultiplier * b ;
  model.w = w ;

  save(conf.modelPath, 'model') ;
else
  load(conf.modelPath) ;
end

% --------------------------------------------------------------------
%                                                Test SVM and evaluate
% --------------------------------------------------------------------

% Estimate the class of the test images
%scores = model.w' * ts_psix + model.b' * ones(1,size(ts_psix,2)) ;
%[drop, imageEstClass] = max(scores, [], 1) ;

[imageEstClass] = predict(ts_imageClass', sparse(double(ts_psix)), svm, '-b 1', 'col');  

% Compute the confusion matrix
%idx = sub2ind([length(ts_classes), length(ts_classes)], ...
%              ts_imageClass(:), imageEstClass(:)) ;
%confus = zeros(length(classes)) ;
%confus = vl_binsum(confus, ones(size(idx)), idx);

gt_label = ts_imageClass;
est_label = imageEstClass;

save('gt_label.mat', 'gt_label');
save('est_label.mat', 'est_label');


% Plots
%figure(1) ; clf;
%subplot(1,2,1) ;
%imagesc(scores(:,[selTrain selTest])) ; title('Scores') ;
%set(gca, 'ytick', 1:length(classes), 'yticklabel', classes) ;
%subplot(1,2,2) ;
%imagesc(confus) ;
%title(sprintf('Confusion matrix (%.2f %% accuracy)', ...
%              100 * mean(diag(confus)/conf.numTest) )) ;
%print('-depsc2', [conf.resultPath '.ps']) ;
%save([conf.resultPath '.mat'], 'confus', 'conf');

% -------------------------------------------------------------------------
function im = standarizeImage(im)
% -------------------------------------------------------------------------

im = im2single(im) ;
if size(im,1) > 480, im = imresize(im, [480 NaN]) ; end

% -------------------------------------------------------------------------
function hist = getImageDescriptor(model, im)
% -------------------------------------------------------------------------

im = standarizeImage(im) ;
width = size(im,2) ;
height = size(im,1) ;
numWords = size(model.vocab, 2) ;

% get PHOW features
[frames, descrs] = vl_phow(im, model.phowOpts{:}) ;

% quantize local descriptors into visual words
switch model.quantizer
  case 'vq'
    [drop, binsa] = min(vl_alldist(model.vocab, single(descrs)), [], 1) ;
  case 'kdtree'
    binsa = double(vl_kdtreequery(model.kdtree, model.vocab, ...
                                  single(descrs), ...
                                  'MaxComparisons', 50)) ;
end

for i = 1:length(model.numSpatialX)
  binsx = vl_binsearch(linspace(1,width,model.numSpatialX(i)+1), frames(1,:)) ;
  binsy = vl_binsearch(linspace(1,height,model.numSpatialY(i)+1), frames(2,:)) ;

  % combined quantization
  bins = sub2ind([model.numSpatialY(i), model.numSpatialX(i), numWords], ...
                 binsy,binsx,binsa) ;
  hist = zeros(model.numSpatialY(i) * model.numSpatialX(i) * numWords, 1) ;
  hist = vl_binsum(hist, ones(size(bins)), bins) ;
  hists{i} = single(hist / sum(hist)) ;
end
hist = cat(1,hists{:}) ;
hist = hist / sum(hist) ;

% -------------------------------------------------------------------------
function [className, score] = classify(model, im)
% -------------------------------------------------------------------------

hist = getImageDescriptor(model, im) ;
psix = vl_homkermap(hist, 1, 'kchi2', 'period', .7) ;
scores = model.w' * psix + model.b' ;
[score, best] = max(scores) ;
className = model.classes{best} ;
