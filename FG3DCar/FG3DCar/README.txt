Here are the dataset (FG3DCar) and some codes used in the paper [1], which proposed to jointly optimizes 3D model fitting and fine-grained classification. 

In this package, we provide the source code for constructing deformable 3D car model and 3D model fitting by landmark-based Jacobian system. 

If you use the data and/or codes, please cite our paper using the following bibtex.

@inproceedings{lin14finegrained3d,
  author    = {Yen-Liang Lin and Vlad I. Morariu and Winston Hsu and Larry S. Davis},
  title     = {Jointly Optimizing 3D Model Fitting and Fine-Grained Classification},
  booktitle = {European Conference on Computer Vision (ECCV)}
  year      = {2014}
}

## Usage

1. visualize ground true annotation
See 'show_ground_truth.m'

2. 3D Model Fitting 
See 'model_fitting.m'

## Third party package

For other components, please download them from the following links:

DPM: http://www.cs.berkeley.edu/~rbg/latent/
SVR: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
Fisher Vector: http://www.vlfeat.org/overview/encodings.html
HOG: http://www.vlfeat.org/overview/hog.html

## References

[1] Yen-Liang Lin, Vlad I. Morariu, Winston Hsu, Larry S. Davis. Jointly Optimizing 3D Model Fitting and Fine-Grained Classification. ECCV 2014




 